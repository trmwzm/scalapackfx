include(common.m4)

dnl ************************************************************************
dnl *** ppotrf
dnl ************************************************************************ 

define(`_subroutine_interface_ppotrf',`
dnl $1: comment
dnl $2: type letter
dnl $3: dummy arguments type
!> Cholesky factorization of symmetric/Hermitian pos.def. matrix ($1).
subroutine p$2potrf(uplo, nn, aa, ia, ja, desca, info)
  import
  character, intent(in) :: uplo
  integer, intent(in) :: nn
  integer, intent(in) :: ia, ja, desca(DLEN_)
  $3, intent(inout) :: aa(desca(LLD_), *)
  integer, intent(out) :: info
end subroutine p$2potrf
')

dnl ************************************************************************
dnl *** psygst
dnl ************************************************************************ 

define(`_subroutine_interface_psygst',`
dnl $1: comment
dnl $2: type letter
dnl $3: dummy arguments kind
!> Reduces generalized symmetric eigenvalue problem to standard form ($1).
subroutine p$2sygst(ibtype, uplo, nn, aa, ia, ja, desca, bb, ib, jb, descb,&
    & scale, info)
  import
  integer, intent(in) :: ibtype
  character, intent(in) :: uplo
  integer, intent(in) :: nn
  integer, intent(in) :: ia, ja, desca(DLEN_)
  real($3), intent(inout) :: aa(desca(LLD_), *)
  integer, intent(in) :: ib, jb, descb(DLEN_)
  real($3), intent(in) :: bb(descb(LLD_), *)
  real($3), intent(out) :: scale
  integer, intent(out) :: info
end subroutine p$2sygst
')

dnl ************************************************************************
dnl *** phegst
dnl ************************************************************************ 

define(`_subroutine_interface_phegst',`
dnl $1: comment
dnl $2: type letter
dnl $3: dummy arguments kind
!> Reduces generalized Hermitian eigenvalue problem to standard form ($1).
subroutine p$2hegst(ibtype, uplo, nn, aa, ia, ja, desca, bb, ib, jb, descb,&
    & scale, info)
  import
  integer, intent(in) :: ibtype
  character, intent(in) :: uplo
  integer, intent(in) :: nn
  integer, intent(in) :: ia, ja, desca(DLEN_)
  complex($3), intent(inout) :: aa(desca(LLD_), *)
  integer, intent(in) :: ib, jb, descb(DLEN_)
  complex($3), intent(in) :: bb(descb(LLD_), *)
  real($3), intent(out) :: scale
  integer, intent(out) :: info
end subroutine p$2hegst
')

dnl ************************************************************************
dnl *** psyev
dnl ************************************************************************ 

define(`_subroutine_interface_psyev',`
dnl $1: comment
dnl $2: type letter
dnl $3: dummy arguments kind
!> Eigenvalues and eigenvectors by divide and conquer algorithm ($1)
subroutine p$2syev(jobz, uplo, nn, aa, ia, ja, desca, ww, zz, iz, jz,&
    & descz, work, lwork, info)
  import
  character, intent(in) :: jobz, uplo
  integer, intent(in) :: nn
  integer, intent(in) :: ia, ja, desca(DLEN_)
  real($3), intent(inout) :: aa(desca(LLD_), *)
  integer, intent(in) :: iz, jz, descz(DLEN_)
  real($3), intent(out) :: ww(nn), zz(descz(LLD_),*)
  real($3), intent(inout) :: work(*)
  integer, intent(in) :: lwork
  integer, intent(out) :: info
end subroutine p$2syev
')

dnl ************************************************************************
dnl *** pheev
dnl ************************************************************************ 

define(`_subroutine_interface_pheev',`
dnl $1: comment
dnl $2: type letter
dnl $3: dummy arguments kind
!> Eigenvalues and eigenvectors by divide and conquer algorithm ($1)
subroutine p$2heev(jobz, uplo, nn, aa, ia, ja, desca, ww, zz, iz, jz,&
    & descz, work, lwork, rwork, lrwork, info)
  import
  character, intent(in) :: jobz, uplo
  integer, intent(in) :: nn
  integer, intent(in) :: ia, ja, desca(DLEN_)
  complex($3), intent(inout) :: aa(desca(LLD_), *)
  integer, intent(in) :: iz, jz, descz(DLEN_)
  real($3), intent(out) :: ww(nn)
  complex($3), intent(out) ::  zz(descz(LLD_),*)
  complex($3), intent(inout) :: work(*)
  integer, intent(in) :: lwork
  real($3), intent(inout) :: rwork(*)
  integer, intent(in) :: lrwork
  integer, intent(out) :: info
end subroutine p$2heev
')

dnl ************************************************************************
dnl *** psyevd
dnl ************************************************************************ 

define(`_subroutine_interface_psyevd',`
dnl $1: comment
dnl $2: type letter
dnl $3: dummy arguments kind
!> Eigenvalues and eigenvectors by divide and conquer algorithm ($1)
subroutine p$2syevd(jobz, uplo, nn, aa, ia, ja, desca, ww, zz, iz, jz,&
    & descz, work, lwork, iwork, liwork, info)
  import
  character, intent(in) :: jobz, uplo
  integer, intent(in) :: nn
  integer, intent(in) :: ia, ja, desca(DLEN_)
  real($3), intent(inout) :: aa(desca(LLD_), *)
  integer, intent(in) :: iz, jz, descz(DLEN_)
  real($3), intent(out) :: ww(nn), zz(descz(LLD_),*)
  real($3), intent(inout) :: work(*)
  integer, intent(in) :: lwork
  integer, intent(inout) :: iwork(*)
  integer, intent(in) :: liwork
  integer, intent(out) :: info
end subroutine p$2syevd
')

dnl ************************************************************************
dnl *** pheevd
dnl ************************************************************************ 

define(`_subroutine_interface_pheevd',`
dnl $1: comment
dnl $2: type letter
dnl $3: dummy arguments kind
!> Eigenvalues and eigenvectors by divide and conquer algorithm ($1)
subroutine p$2heevd(jobz, uplo, nn, aa, ia, ja, desca, ww, zz, iz, jz,&
    & descz, work, lwork, rwork, lrwork, iwork, liwork, info)
  import
  character, intent(in) :: jobz, uplo
  integer, intent(in) :: nn
  integer, intent(in) :: ia, ja, desca(DLEN_)
  complex($3), intent(inout) :: aa(desca(LLD_), *)
  integer, intent(in) :: iz, jz, descz(DLEN_)
  real($3), intent(out) :: ww(nn)
  complex($3), intent(out) ::  zz(descz(LLD_),*)
  complex($3), intent(inout) :: work(*)
  integer, intent(in) :: lwork
  real($3), intent(inout) :: rwork(*)
  integer, intent(in) :: lrwork
  integer, intent(inout) :: iwork(*)
  integer, intent(in) :: liwork
  integer, intent(out) :: info
end subroutine p$2heevd
')

dnl ************************************************************************
dnl *** ptrsm
dnl ************************************************************************ 

define(`_subroutine_interface_ptrsm',`
dnl $1: comment
dnl $2: type letter
dnl $3: dummy arguments type
!> Solves a triangular matrix equation ($1).
subroutine p$2trsm(side, uplo, transa, diag, mm, nn, alpha, aa, ia, ja,&
    & desca, bb, ib, jb, descb)
  import
  character, intent(in) :: side, uplo, transa, diag
  integer, intent(in) :: mm, nn
  $3, intent(in) :: alpha
  integer, intent(in) :: desca(DLEN_)
  $3, intent(in) :: aa(desca(LLD_), *)
  integer, intent(in) :: ia, ja
  integer, intent(in) :: descb(DLEN_)
  $3, intent(inout) :: bb(descb(LLD_), *)
  integer, intent(in) :: ib, jb
end subroutine p$2trsm
')
